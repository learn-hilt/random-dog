package dali.hmida.randomdog.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}