package dali.hmida.randomdog.ui.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.ActionBar
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import dali.hmida.randomdog.R
import dali.hmida.randomdog.databinding.ActivityMainBinding
import dali.hmida.randomdog.ui.viewModel.DogViewModel
import dali.hmida.randomdog.utils.Status


@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val dogViewModel : DogViewModel by viewModels()
    private var _binding : ActivityMainBinding? = null
    private val binding get() = _binding!!
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.dogVM = dogViewModel
        binding.lifecycleOwner = this
        setupObserver()
    }


    private fun setupObserver() {
        dogViewModel.dog.observe(this ){
            when (it.status) {
                Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    it.data?.let { dog -> showImage(dog.message) }
                    binding.dogImage.visibility = View.VISIBLE
                }
                Status.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                    binding.dogImage.visibility = View.GONE
                }
                Status.ERROR -> {
                    //Handle Error
                    binding.progressBar.visibility = View.GONE
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    private fun showImage(dogImage: String){
        Glide.with(binding.dogImage.context)
            .load(dogImage)
            .into(binding.dogImage)

    }
}