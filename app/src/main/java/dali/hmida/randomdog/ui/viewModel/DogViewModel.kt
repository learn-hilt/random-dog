package dali.hmida.randomdog.ui.viewModel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import dali.hmida.randomdog.data.model.Dog
import dali.hmida.randomdog.data.repository.DogRepository
import dali.hmida.randomdog.utils.NetworkHelper
import dali.hmida.randomdog.utils.Resource
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DogViewModel @Inject constructor(
    private val mainRepository: DogRepository,
    private val networkHelper: NetworkHelper
) : ViewModel() {



    private val _dog = MutableLiveData<Resource<Dog>>()
    val dog: LiveData<Resource<Dog>>
        get() = _dog

    init {
        fetchRandomDog()
    }




    fun fetchRandomDog(){
        Log.e("daa","dd")
        viewModelScope.launch {
            _dog.postValue(Resource.loading(null))
            if (networkHelper.isNetworkConnected()) {
                mainRepository.getRandomDog().let {
                    if (it.isSuccessful) {
                        _dog.postValue(Resource.success(it.body()))
                    } else _dog.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else _dog.postValue(Resource.error("No internet connection", null))

        }

    }


}