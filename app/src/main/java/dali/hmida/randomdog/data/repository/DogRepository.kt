package dali.hmida.randomdog.data.repository

import dali.hmida.randomdog.data.api.ApiHelper
import javax.inject.Inject

class DogRepository @Inject constructor(private val apiHelper: ApiHelper) {
    suspend fun getRandomDog() =  apiHelper.getRandomDog()
}