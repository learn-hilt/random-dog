package dali.hmida.randomdog.data.api

import dali.hmida.randomdog.data.model.Dog
import retrofit2.Response
import javax.inject.Inject

class ApiHelperImpl @Inject constructor(private val apiService: ApiService) : ApiHelper {
    override suspend fun getRandomDog(): Response<Dog> = apiService.getRandomDog()
}