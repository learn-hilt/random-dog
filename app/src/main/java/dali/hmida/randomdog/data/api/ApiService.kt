package dali.hmida.randomdog.data.api

import dali.hmida.randomdog.data.model.Dog
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {

    @GET("breeds/image/random")
    suspend fun getRandomDog(): Response<Dog>

}