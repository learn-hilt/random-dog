package dali.hmida.randomdog.data.api

import dali.hmida.randomdog.data.model.Dog
import retrofit2.Response

interface ApiHelper {

    suspend fun getRandomDog(): Response<Dog>

}